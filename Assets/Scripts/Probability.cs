﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Probability
{
    public static bool DetectFromPercent(int percent)
    {
        return DetectFromPercent((float)percent);
    }

    public static bool DetectFromPercent(float percent)
    {
        int digitNum = 0;
        if (percent.ToString().IndexOf(".") > 0)
        {
            digitNum = percent.ToString().Split('.')[1].Length;
        }

        int rate = (int)Mathf.Pow(10, digitNum);

        int randomValueLimit = 100 * rate;
        int border = (int)(rate * percent);

        return Random.Range(0, randomValueLimit) < border;
    }

    public static T DetermineFromDict<T>(Dictionary<T, int> targetDict)
    {
        Dictionary<T, float> targetFloatDict = new Dictionary<T, float>();

        foreach (KeyValuePair<T, int> pair in targetDict)
        {
            targetFloatDict.Add(pair.Key, (float)pair.Value);
        }

        return DetermineFromDict(targetFloatDict);
    }


    public static T DetermineFromDict<T>(Dictionary<T, float> targetDict)
    {

        float totalPer = 0;
        foreach (float per in targetDict.Values)
        {
            totalPer += per;
        }

        float rand = Random.Range(0, totalPer);


        foreach (KeyValuePair<T, float> pair in targetDict)
        {
            rand -= pair.Value;

            if (rand <= 0)
            {
                return pair.Key;
            }
        }

        return new List<T>(targetDict.Keys)[0];
    }
}
