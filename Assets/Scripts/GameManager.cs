﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR 
using UnityEditor;
#endif

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public float PlayerSpeed;
    private Vector3 MousePosition;
    public Vector3 Velocity;
    public int CurrentStage;
    public Button button;
    public DeadZone DeadZone;
    public Text text;
    public State CurrentState
    {
        get; private set;
    }

    public enum State
    {
        GameStart,
        Prepare,
        Play,
        GameEnd
    }
    private void Awake()
    {
        if(Instance != null)
        {
            GameObject.Destroy(this);
            return;
        }
        Instance = this;
        if(DeadZone == null)
        {
            DeadZone = GetComponent<DeadZone>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentState = State.GameStart;
        CurrentStage = 0;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
        text.text = "Stage " + (CurrentStage+1) ;
    }

    public void UpdateState()
    {
        switch (CurrentState)
        {
            case State.GameStart:
                button.gameObject.SetActive(true);
                break;
            case State.Prepare:
                StartCoroutine(PrepareCoroutine());
                break;
            case State.Play:
                break;
            case State.GameEnd:
                GameEnd();
                break;
        }
    }
    public void ChangeState(State state)
    {
        CurrentState = state;
    }
    public void GameStart()
    {
        CurrentStage++;
        ChangeState(State.Prepare);
        button.gameObject.SetActive(false);
    }

    private void Prepare()
    {

    }

    private void Play()
    {

    }

    private void GameEnd()
    {
        #if UNITY_EDITOR
        EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
    public IEnumerator PrepareCoroutine()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(State.Play);
    }
    public State GetCurrentState()
    {
        return CurrentState;
    }
}
