﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DeadZone : MonoBehaviour
{
    public GameObject DeadZoneBlock,Flag;
    public Transform position;
    public RectTransform rectTransform;
    public GridLayoutGroup gridLayoutGroup;
    public List<GameObject> DeadZoneBlocks;
    public GameManager GameManager;
    private int width;

    private void Awake()
    {
        if(position == null)
        {
            position = GetComponent<Transform>();
        }
        if(rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
        if(gridLayoutGroup== null)
        {
            gridLayoutGroup = GetComponent<GridLayoutGroup>();
        }
        if (GameManager == null)
        {
            GameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    private void Initialize()
    {
        width = (int)(rectTransform.sizeDelta.x / gridLayoutGroup.cellSize.x);
        int rand = Random.Range(1, width - 1);
        if (DeadZoneBlocks.Count < width)
        {
            for (int i = 0; i < width; i++)
            {
                if (i != rand)
                {
                    DeadZoneBlocks.Add(Instantiate(DeadZoneBlock, position));
                }
                else if (i == rand)
                {
                    DeadZoneBlocks.Add(Instantiate(Flag, position));
                }
            }
        }
    }

    private void UpdateState()
    {
        switch (GameManager.Instance.CurrentState)
        {
            case GameManager.State.GameStart:
                break;
            case GameManager.State.Prepare:
                SetFlag();
                break;
            case GameManager.State.Play:
                break;
            case GameManager.State.GameEnd:
                DeactivateBlocks();
                break;
        }
        Debug.Log(GameManager.Instance.CurrentState);
    }

    public void SetFlag()
    {
        int rand = Random.Range(1, width - 1);
        int flag = DeadZoneBlocks.IndexOf(GameObject.FindGameObjectWithTag("Finish"));
        GameObject obj = DeadZoneBlocks[flag];
        DeadZoneBlocks[flag] = DeadZoneBlocks[rand];
        DeadZoneBlocks[flag].transform.SetSiblingIndex(rand);
        DeadZoneBlocks[rand] = obj;
        DeadZoneBlocks[rand].transform.SetSiblingIndex(flag);
        Debug.Log(DeadZoneBlocks[rand]);
    }

    public void DeactivateBlocks()
    {
        for(int i = 0; i < DeadZoneBlocks.Count; i++)
        {
            DeadZoneBlocks[i].SetActive(false);
        }
    }
}
