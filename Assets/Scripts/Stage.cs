﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stage : MonoBehaviour
{
    public GameManager GameManager;
    public GameObject Block;
    public Transform position;
    public RectTransform rectTransform;
    public GridLayoutGroup gridLayoutGroup;
    public List<GameObject> Blocks;
    // Start is called before the first frame update
    private void Awake()
    {
        if (position == null)
        {
            position = GetComponent<Transform>();
        }
        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
        if(gridLayoutGroup == null)
        {
            gridLayoutGroup = GetComponent<GridLayoutGroup>();
        }
        if(GameManager == null)
        {
            GameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        }
    }
    void Start()
    {
        int BlockNumber = (int)(rectTransform.sizeDelta.x / gridLayoutGroup.cellSize.x) * (int)(rectTransform.sizeDelta.y / gridLayoutGroup.cellSize.y);
        for(int i = 0; i <= BlockNumber; i++)
        {
            Blocks.Add(Instantiate(Block,position));
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        switch (GameManager.Instance.CurrentState)
        {
            case GameManager.State.GameStart:
                break;
            case GameManager.State.Prepare:
                ActivateBlocks();
                break;
            case GameManager.State.Play:
                break;
            case GameManager.State.GameEnd:
                DeactivateBlocks();
                break;
        }
        Debug.Log(GameManager.Instance.CurrentState);
    }

    private void ActivateBlocks()
    {
        for(int i = 0; i < Blocks.Count; i++)
        {
            Debug.Log("Block / " + Blocks[i].activeSelf);
            if (!Blocks[i].transform.GetChild(0).gameObject.activeSelf)
            {
                Blocks[i].transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    private void DeactivateBlocks()
    {
        for (int i = 0; i < Blocks.Count; i++)
        {
             Blocks[i].SetActive(false);
        }
    }
}
