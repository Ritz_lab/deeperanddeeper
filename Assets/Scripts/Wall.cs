﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wall : MonoBehaviour
{
    public GameObject DeadZoneBlock;
    public Transform position;
    public RectTransform rectTransform;
    public GridLayoutGroup gridLayoutGroup;

    private void Awake()
    {
        if (position == null)
        {
            position = GetComponent<Transform>();
        }
        if (rectTransform == null)
        {
            rectTransform = GetComponent<RectTransform>();
        }
        if (gridLayoutGroup == null)
        {
            gridLayoutGroup = GetComponent<GridLayoutGroup>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(DeadZoneBlock, position);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
