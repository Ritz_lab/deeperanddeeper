﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Block : MonoBehaviour
{
    public GameManager GameManager;
    public bool IsPushed;
    public float BreakTime = 3f;
    public float CurrentTime;
    public Image BlockImage,Frame;
    public Color CurrentColor;
    private Color[] Colors = {
        new Color(51f / 255f, 0f, 8f / 255f),
        new Color(183f / 255f, 0f, 30f / 255f),
        new Color(234f / 255, 0f, 39f / 255f),
        new Color(1f, 32f / 255f, 67f / 255f),
        new Color(1f, 81f / 255f, 110f / 255f),
        new Color(1f, 132f / 255f, 153f / 255f),
    };

    public enum ToughNess
    {
        UnBreakable,
        UltraRare,
        SuperRare,
        Rare,
        UnCommon,
        Common,
    }
    public ToughNess CurrentToughNess;
    private void Awake()
    {

        if(BlockImage == null)
        {
            BlockImage = GetComponent<Image>();
        }
        if(Frame == null)
        {
            Frame = GetComponentInParent<Image>();
        }
        if(GameManager == null)
        {
            GameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    private void OnEnable()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.Instance.CurrentState == GameManager.State.Prepare)
        {
            BlockImage.fillAmount = 1f;
        }
        else if (GameManager.Instance.CurrentState == GameManager.State.Play)
        {
            if (CurrentToughNess != ToughNess.UnBreakable)
            {
                if (IsPushed)
                {
                    CurrentTime += Time.deltaTime;
                }
                else if (!IsPushed)
                {
                    CurrentTime = 0;
                }
                BlockImage.fillAmount = 1f - CurrentTime / BreakTime;
                Frame.fillAmount = 1f - CurrentTime / BreakTime;
                if (CurrentTime >= BreakTime)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
    private void Initialize()
    {
        IsPushed = false;
        CurrentTime = 0;
        CurrentToughNess = SetToughNess(GameManager.Instance.CurrentStage);
        BreakTime = GetBreakTime();
        CurrentColor = SetColor();
        BlockImage.color = CurrentColor;
    }

    private float GetBreakTime()
    {
        float temp = 0;
        switch (CurrentToughNess)
        {
            case ToughNess.Common:
                temp = 1f;
                break;
            case ToughNess.UnCommon:
                temp = 1.2f;
                break;
            case ToughNess.Rare:
                temp = 1.5f;
                break;
            case ToughNess.SuperRare:
                temp = 2.0f;
                break;
            case ToughNess.UltraRare:
                temp = 2.5f;
                break;
            default:
                temp = 10f;
                break;
        }
        return temp;
    }

    private Color SetColor()
    {
        return Colors[(int)CurrentToughNess];
    }

    private ToughNess SetToughNess(int CurrentStage)
    {
        Dictionary<ToughNess, int> Dict = new Dictionary<ToughNess, int>()
        {
            {ToughNess.Common,60 - CurrentStage * CurrentStage },
            {ToughNess.UnCommon,50 - CurrentStage * 2 },
            {ToughNess.Rare,40 - CurrentStage },
            {ToughNess.SuperRare,20 + CurrentStage * 2 },
            {ToughNess.UltraRare,10 + CurrentStage },
            {ToughNess.UnBreakable,5 + (int)(CurrentStage / 2) },
        };
        ToughNess nextToughNess = Probability.DetermineFromDict<ToughNess>(Dict);
        return nextToughNess;
    }

    public void PushUp()
    {
        IsPushed = false;
    }

    public void PushDown()
    {
        IsPushed = true;
    }
}
