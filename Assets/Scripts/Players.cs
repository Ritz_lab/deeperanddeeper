﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Players : MonoBehaviour
{
    public GameObject Player1,Player2;
    public List<GameObject> Player;
    public int BaseCount = 1;
    public GridLayoutGroup gridLayoutGroup;
    public Transform position;
    public int TotalCount;
    private int GoalPlayer;
    public bool Prepared;
    public GameManager GameManager;

    private void Awake()
    {
        if(position == null)
        {
            position = GetComponent<Transform>();
        }
        if(gridLayoutGroup == null)
        {
            gridLayoutGroup = GetComponent<GridLayoutGroup>();
        }
        if(GameManager == null)
        {
            GameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        switch (GameManager.Instance.CurrentState)
        {
            case GameManager.State.GameStart:
                break;
            case GameManager.State.Prepare:
                Initialize();
                break;
            case GameManager.State.Play:
                Check();
                break;
            case GameManager.State.GameEnd:
                break;
        }
        Debug.Log(GameManager.Instance.CurrentState);

    }

    private void Initialize()
    {
        TotalCount = BaseCount + GameManager.Instance.CurrentStage * 2;
        if(Player.Count == 0)
        {
            Player.Add(Instantiate(Player1, position));
        }
        else if(Player.Count >= 1 && Player.Count <= TotalCount)
        {
            for (int i = 0; i < TotalCount - Player.Count; i++)
            {
                if (Player[Player.Count - 1].CompareTag("SecondPlayer"))
                {
                    Player.Add(Instantiate(Player1, position));
                }
                else if(Player[Player.Count - 1].CompareTag("Player"))
                {
                    Player.Add(Instantiate(Player2, position));
                }
            }
        }
        for(int i = 0; i < Player.Count; i++)
        {
            Debug.Log("Player / " + Player[i].activeSelf);
            if (!Player[i].activeSelf)
            {
                Player[i].SetActive(true);
                Player[i].transform.position = position.position;
            }
        }
    }

    private void Check()
    {
        GoalPlayer = 0;
        for(int i = 0; i < Player.Count; i++)
        {
            if (!Player[i].activeSelf)
            {
                GoalPlayer++;
            }
        }
        if(GoalPlayer == Player.Count)
        {
            GameManager.Instance.ChangeState(GameManager.State.GameStart);
        }
    }
}
