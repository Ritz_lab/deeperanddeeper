﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float Jump = 5f;
    public Canvas Canvas;
    public float PlayerSpeed = 1f;
    public Vector3 MousePosition;
    public Vector3 Velocity;
    public RectTransform canvasRect;
    public Rigidbody2D rigidBody2D;
    public bool OnGround;
    public int PlayerNumber;
    // Start is called before the first frame update
    private void Awake()
    {
        if(Canvas == null)
        {
            Canvas = transform.root.GetComponent<Canvas>();
        }
        canvasRect = Canvas.GetComponent<RectTransform>();
        if(rigidBody2D == null)
        {
            rigidBody2D = GetComponent<Rigidbody2D>();
        }
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            //MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            MousePosition = Input.mousePosition;
            var magnification = canvasRect.sizeDelta.x / Screen.width;
            MousePosition.x = MousePosition.x * magnification - canvasRect.sizeDelta.x / 2;
            MousePosition.y = MousePosition.y * magnification - canvasRect.sizeDelta.y / 2;
            MousePosition.z = transform.localPosition.z;
            //Velocity = (Camera.main.ScreenToWorldPoint(MousePosition) - transform.position).normalized;
            Velocity = (MousePosition - transform.position).normalized;
            Velocity = new Vector3(Velocity.x, 0, 0);
            if (this.gameObject.tag == "Player")
            {
                transform.position += Velocity * PlayerSpeed * Time.deltaTime;
            }else if(this.gameObject.tag == "SecondPlayer")
            {
                transform.position -= Velocity * PlayerSpeed * Time.deltaTime;
            }
            //Debug.Log(transform.position);
        }
        if (Input.GetMouseButtonDown(1) && OnGround)
        {
            rigidBody2D.AddForce(Vector2.up * Jump);
            OnGround = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            OnGround = true;
        }else if(collision.gameObject.tag == "Finish")
        {
            Debug.Log("Finish");
            gameObject.SetActive(false);
        }
        else if(collision.gameObject.tag == "DeadZone")
        {
            Debug.Log("Dead");
            //gameObject.SetActive(false);
            GameManager.Instance.ChangeState(GameManager.State.GameEnd);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            OnGround = false;
        }
    }
}
